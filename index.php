<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Formulaire</title>
    
</head>
<body>
    <?php include "connexion.php"; 
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    ?> 

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <div class="container">
    <h1>BEAU DE L'HAIR</h1>
    <form name="monFormulaire" method="POST" action="ajouter.php">
    <input class="form-control ml-3 mb-3" type="date" id="datereserve" name="datereserve" size="10" aria-label="AFFICHER">
    <div id="heures"></div>
    <script>

        <?php
        $reponse = $pdo->query('SELECT * FROM client');
        $crenaux_reserve =  json_encode($reponse->fetchAll()); 
        ?>

        let crenaux_reserve = <?php echo $crenaux_reserve; ?>

        let horaire = []
            horaire[0] = []
            horaire[1] =[]
            horaire[2] = [9, 10, 11, 13, 14, 15, 16, 17]
            horaire[3] = [9, 10, 11, 13, 14, 15, 16, 17]
            horaire[4] = [9, 10, 11, 13, 14, 15, 16, 17]
            horaire[5] = [9, 10, 11, 13, 14, 15, 16, 17]
            horaire[6] = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        
        let dte = document.getElementById("datereserve")
        let date;
        let horaire_du_jour;
        dte.addEventListener("change", (e)=>{
            date = dte.value
            let num_du_jour = new Date(date).getDay()
            horaire_du_jour = horaire[num_du_jour]
            let bob = ""
            
            for(let c of horaire_du_jour){
                bob += c
                bob += "h00"
                bob += `<input type="radio" value="${c}" name="heure">`
            }
            
            document.getElementById('heures').innerHTML = bob
        })
         </script>


            <div>
            <div class="row1">
                Nom 
                <input type="text" name="nom">
            </div>
            <div class="row1">
                Prénom
                <input type="text" name="prenom">
            </div>
            <div class="row1">
                Teléphone
                <input type="text" name="tel">
            </div>
            <div class="row1">
                Email
                <input type="text" name="mail">
            </div>
            <div class="row1"> 
            <input type="checkbox" name="rgpd" value=1 > J'accepte le rgpd 
            </div>
            <div class="row1">  
                <button type="submit" name="valider">valider</button> 
            </div>
        </form>
        
    </div>
 
</body>
</html>